// main.qml
// import QtQuick 2.0
import QtQuick 2.4
import QtQuick.Layouts 1.0
// import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents

//import userbutton.qml

Item {
    id: widget

ColumnLayout {
    RowLayout {
        PlasmaComponents.Button {
            id: userbutton
            icon.name: "system-users"
            text: "Users"
        }

        PlasmaComponents.TextField {
            id: search
            placeholderText: qsTr("Search")
        }
    }

    RowLayout {
        PlasmaComponents.Button {
            id: shutdownbutton
            icon.name: "system-shut-down"
            text: "Shutdown"
        }
    }
}
}
